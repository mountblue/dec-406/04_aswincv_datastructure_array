/*
Name of the project : Datastructure_matrices
File name : array_1/sum.js
Description : finds the sum of elements and subtract eaach elemnt from the sum.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : 1,2,3
Output : 5,4,3
*/


function sum(input){
    if (toString.call(input) !== "[object Array]")
        return false;
    var total =  0;
    var output = [];
    for(var i=0;i<input.length;i++){
        if(isNaN(input[i])){
            continue;
        }
        total += Number(input[i]);
     }
     for(let i=0; i<input.length;i++){
       output[i] = total-input[i];
     }
     return output;
}


console.log(sum([1,2,3]));
console.log(sum([100,-200,3]));
console.log(sum([1,2,'a',3]));
