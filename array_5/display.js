/*
Name of the project : Datastructure_matrices
File name : array_5/display.js
Description : finds the pair whose sum is closest to x and the
              pair has an element from each array.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,4,5,7],[10,20,30,40],32
Output : 1 30
*/


var close = 0;
var change;

//to find the which arrays sum is close to the given input
function find(arr1,arr2,k) {
    close  = arr1[0];
    change = Math.abs(k-close);
    var sum1,sum2;
    sum1 = sum(arr1,k);
    sum2 = sum(arr2,k);
    if(Math.abs(k-sum1) < Math.abs(k-sum2)) {
        console.log("1");
    }else {
        console.log("2");
    }
    console.log(close);
}

//finds the sum of the elements
function sum(input,k){
    var total =  0;
    var output = [];
    for(var i=0;i<input.length;i++){
        total += Number(input[i]);
        if(Math.abs(k-input[i])<change) {
            close = input[i];
            change = Math.abs(k-input[i]);
        }
     }
     for(let i=0; i<input.length;i++){
       output[i] = total-input[i];
     }
     return output;
}


console.log(find([1,4,5,7],[10,20,30,40],32));
