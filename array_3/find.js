/*
Name of the project : Datastructure_matrices
File name : array_3/find.js
Description : finds the nth missing element.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,3,4,5,7],5,2
Output : 6
*/


var missing = [];

//function to find the missing element
function find(input,n,k) {
    var flag = false;
    missing.length = 0;
    for (var i = 1; i < input.length; i++) {
      if(input[i]!=(input[i-1]+1)) {
        missing.push((input[i-1]+1));
        flag = true;
      }
    }
    if(flag) {
      return missing[--k];
    }else {
      return -1;
    }
}


var arr = [1,3,4,5,7];
var arr1 = [1,2,3,4,5,6]
console.log(find(arr,5,2));
console.log(find(arr1,6,1));
